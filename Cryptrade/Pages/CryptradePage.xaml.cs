﻿using Xamarin.Forms;

namespace Cryptrade.Pages
{
    public partial class CryptradePage : ContentPage
    {
        void Ency_Clicked(object sender, System.EventArgs e)
        {
            var encyclopediaPage = new NavigationPage(new Pages.Encyclopedia());
            Navigation.PushAsync(encyclopediaPage);
        }

        void Chart_Clicked(object sender, System.EventArgs e)
        {
            var chartPage = new NavigationPage(new Pages.Charts());
            Navigation.PushAsync(chartPage);
            //DisplayAlert("Alert", "This is an image button", "OK");  
        }

        protected override void OnAppearing()
        {
            InitializeComponent();
            BindingContext = new ExchangeViewModel();
            Picker1.SelectedIndex = 0;

            var encImage = new TapGestureRecognizer();
            var chartImage = new TapGestureRecognizer();
            //Binding events  
            encImage.Tapped += Ency_Clicked;
            chartImage.Tapped += Chart_Clicked;
            //Associating tap events to the image buttons  
            encyclopedia.GestureRecognizers.Add(encImage);
            chart.GestureRecognizers.Add(chartImage);
        }

        public CryptradePage()
        {
            //InitializeComponent();
            //BindingContext = new ExchangeViewModel();


        }
    }
}
