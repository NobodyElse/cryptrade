﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using CarouselView.FormsPlugin.Abstractions;
using Cryptrade.ViewModels;

using Xamarin.Forms;

namespace Cryptrade.Pages
{
    public partial class Encyclopedia : ContentPage
    {
        EncyclopediaViewModel _vm;

        public Encyclopedia()
        {
            InitializeComponent();
            BindingContext = _vm = new EncyclopediaViewModel();

        }

        void Handle_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            Debug.WriteLine("Position " + e.NewValue + " selected.");
        }

        void Handle_Scrolled(object sender, CarouselView.FormsPlugin.Abstractions.ScrolledEventArgs e)
        {
            Debug.WriteLine("Scrolled to " + e.NewValue + " percent.");
            Debug.WriteLine("Direction = " + e.Direction);
        }

    }
}
