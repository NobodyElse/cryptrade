﻿using Microcharts;
using SkiaSharp;
using System.Collections.Generic;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace Cryptrade.Pages
{
    public partial class Charts : ContentPage
    {
        //List<Entry> entries = new List<Entry>
        //{
        //    new Entry(212)
        //    {
        //        Label = "UWP",
        //        ValueLabel = "212",
        //        Color = SKColor.Parse("#2c3e50")
        //    },
        //    new Entry(248)
        //    {
        //        Label = "Android",
        //        ValueLabel = "248",
        //        Color = SKColor.Parse("#77d065")
        //    },
        //    new Entry(128)
        //    {
        //        Label = "iOS",
        //        ValueLabel = "128",
        //        Color = SKColor.Parse("#b455b6")
        //    },
        //    new Entry(514)
        //    {
        //        Label = "Shared",
        //        ValueLabel = "514",
        //        Color = SKColor.Parse("#3498db")
        //    }
        //};

        public Charts()
        {
            InitializeComponent();
            BindingContext = new ChartViewModel();
            var entries = new ChartViewModel().Entries;
            //var chart = new LineChart()
            //{
            //    Entries = entries,
            //    LineMode = LineMode.Straight,
            //    LineSize = 8,
            //    PointMode = PointMode.Square,
            //    PointSize = 18
            //};

            //this.chartView.Chart = chart;

            //Chart1.Chart = new RadialGaugeChart() { Entries = entries };
        }
    }
}
