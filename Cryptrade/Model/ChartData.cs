﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cryptrade.RestServices;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms.Internals;

namespace Cryptrade.Model
{
    public class ChartData
    {
        public static IList<ChartModel> Charts { get; set; }
        public static IList<ChartTimeModel> ChartTimeList { get; set; }
        public static IList<Entry> Entries30D { get; set; }
        public static IList<Entry> Entries7D { get; set; }
        public static Lazy<IList<Entry>> Entries7DLazy { get; set; }
        public static IList<Entry> Entries24H { get; set; }
        public static Lazy<IList<Entry>> lazyOrders = new Lazy<IList<Entry>>();

        private static List<Datum> CryptoConvertCA(string crypto, string fiat, int days)
        {
            RestServiceCA restService = new RestServiceCA();
            var convCryptoFiat = Task.Run(() => restService.CheckCryptoFiatAsyncCA(crypto, fiat, days)).Result;
            return convCryptoFiat.Data;
        }

        private static List<Datum> CryptoConvertCAHours(string crypto, string fiat)
        {
            RestServiceCA restService = new RestServiceCA();
            var convCryptoFiat = Task.Run(() => restService.CheckCryptoFiatAsyncCAHours(crypto, fiat)).Result;
            return convCryptoFiat.Data;
        }

        private static string epoch2string(long epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(epoch).ToLocalTime().ToString();
        }

        private static List<Entry> PopulateListXDays (string crypto, string fiat,  int days){
            var entriesMonth = new List<Entry>();
            var count = CryptoConvertCA(crypto, fiat, days).Count;
            for (int n = 0; n < count; n++)
            {
                entriesMonth.Add(new Entry(((CryptoConvertCA(crypto, fiat, days))[n].High))
                {
                    Label = epoch2string((CryptoConvertCA(crypto, fiat, days))[n].Time),
                    ValueLabel = ((CryptoConvertCA(crypto, fiat, days))[n].High).ToString(),
                    Color = SKColor.Parse("#77d065")
                });
            }
            return entriesMonth;
        }

        private static List<Entry> PopulateList24Hours(string crypto, string fiat)
        {
            var entriesHours = new List<Entry>();
            var count = CryptoConvertCAHours(crypto, fiat).Count;
            for (int n = 0; n < count; n++)
            {
                entriesHours.Add(new Entry(((CryptoConvertCAHours(crypto, fiat))[n].High))
                {
                    Label = epoch2string((CryptoConvertCAHours(crypto, fiat))[n].Time),
                    ValueLabel = ((CryptoConvertCAHours(crypto, fiat))[n].High).ToString(),
                    Color = SKColor.Parse("#77d065")
                });
            }
            return entriesHours;
        }
        static ChartData()
        {

            //Entries.Add(new Entry(((CryptoConvertCA("BTC", "USD"))[0].High)) 
            //{ 
            //    Label = epoch2string((CryptoConvertCA("BTC", "USD"))[0].Time),
            //    ValueLabel = ((CryptoConvertCA("BTC", "USD"))[0].High).ToString(),
            //    Color = SKColor.Parse("#77d065")
            //});
            //Entries.Add(new Entry(((CryptoConvertCA("BTC", "USD"))[1].High))
            //{
            //    Label = epoch2string((CryptoConvertCA("BTC", "USD"))[1].Time),
            //    ValueLabel = ((CryptoConvertCA("BTC", "USD"))[1].High).ToString(),
            //    Color = SKColor.Parse("#77d065")
            //});
            //Entries.Add(new Entry(((CryptoConvertCA("BTC", "USD"))[2].High))
            //{
            //    Label = epoch2string((CryptoConvertCA("BTC", "USD"))[2].Time),
            //    ValueLabel = ((CryptoConvertCA("BTC", "USD"))[2].High).ToString(),
            //    Color = SKColor.Parse("#77d065")
            //});
            //Entries.Add(new Entry(((CryptoConvertCA("BTC", "USD"))[3].High))
            //{
            //    Label = epoch2string((CryptoConvertCA("BTC", "USD"))[3].Time),
            //    ValueLabel = ((CryptoConvertCA("BTC", "USD"))[3].High).ToString(),
            //    Color = SKColor.Parse("#77d065")
            //});
            //Entries30D = PopulateListXDays("BTC", "USD", 30);


            Entries7D = PopulateListXDays("BTC", "USD", 7);
            Entries24H = PopulateList24Hours("BTC", "USD");

            //var chart30D = new LineChart()
            //{
            //    Entries = Entries30D,
            //    LineMode = LineMode.Straight,
            //    LineSize = 8,
            //    PointMode = PointMode.Square,
            //    PointSize = 18
            //};

            var chart7D = new LineChart()
            {
                Entries = Entries7D,
                LineMode = LineMode.Straight,
                LineSize = 8,
                PointMode = PointMode.Square,
                PointSize = 18
            };

            var chart24H = new LineChart()
            {
                Entries = Entries24H,
                LineMode = LineMode.Straight,
                LineSize = 8,
                PointMode = PointMode.Square,
                PointSize = 18
            };

            ChartTimeList = new List<ChartTimeModel>();
            ChartTimeList.Add(new ChartTimeModel()
            {
                Time = "24Hours",
                Chart = chart24H
            });
            ChartTimeList.Add(new ChartTimeModel()
            {
                Time = "7Days",
                Chart = chart7D
            });
            //ChartTimeList.Add(new ChartTimeModel()
            //{
            //    Time = "30Days",
            //    Chart = chart30D
            //});


            Charts = new List<ChartModel>();
            Charts.Add(new ChartModel()
            {
                Exchange = "BTC",
                ChartTime = ChartTimeList

            });
            Charts.Add(new ChartModel()
            {
                Exchange = "ETH",
                ChartTime = ChartTimeList
            });
            Charts.Add(new ChartModel()
            {
                Exchange = "LSK",
                ChartTime = ChartTimeList
            });

        }
    }
}
