﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Cryptrade.RestServices;

namespace Cryptrade.Model
{
    public class ExchangeData
    {
        public static IList<ExchangeModel> Exchanges { get; set; }
        public static IList<CurrencyModel> CurrenciesBitBay { get; set; }
        public static IList<CurrencyModel> CurrenciesBitStamp { get; set; }
        public static IList<CurrencyModel> CurrenciesLivecoin { get; set; }
        public static IList<CurrencyModel> CurrenciesGemini { get; set; }

        //var convBTCUSD = Task.Run(() => restService.CheckBTCUSDAsync()).Result;
        public static double CryptoConvertBB(string crypto, string fiat) 
        {
            RestServiceBB restService = new RestServiceBB();
            var convCryptoFiat = Task.Run(() => restService.CheckCryptoFiatAsyncBB(crypto, fiat)).Result;
            return convCryptoFiat.Average;
        }

        public static double CryptoConvertBS(string crypto, string fiat)
        {
            RestServiceBS restService = new RestServiceBS();
            var convCryptoFiat = Task.Run(() => restService.CheckCryptoFiatAsyncBS(crypto, fiat)).Result;
            return double.Parse(convCryptoFiat.Vwap);
        }

        public static double CryptoConvertLC(string crypto, string fiat)
        {
            RestServiceLC restService = new RestServiceLC();
            var convCryptoFiat = Task.Run(() => restService.CheckCryptoFiatAsyncLC(crypto, fiat)).Result;
            return Math.Round(convCryptoFiat.Vwap, 2);
        }

        public static double CryptoConvertGE(string crypto, string fiat)
        {
            RestServiceGE restService = new RestServiceGE();
            var convCryptoFiat = Task.Run(() => restService.CheckCryptoFiatAsyncGE(crypto, fiat)).Result;
            return double.Parse(convCryptoFiat.Ask);
        }

        public static double ConvertNBP(string currency)
        {
            RestServiceNBP restService = new RestServiceNBP();
            var convCryptoFiat = Task.Run(() => restService.CheckNBP(currency)).Result;
            return convCryptoFiat.Rates[0].Mid;
        }

        static ExchangeData()
        {
            CurrenciesBitBay = new List<CurrencyModel>();
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BTC/USD", CValue = CryptoConvertBB("BTC", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BTC/EUR", CValue = CryptoConvertBB("BTC", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BTC/PLN", CValue = CryptoConvertBB("BTC", "PLN") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BCC/USD", CValue = CryptoConvertBB("BCC", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BCC/EUR", CValue = CryptoConvertBB("BCC", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BCC/PLN", CValue = CryptoConvertBB("BCC", "PLN") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "LTC/USD", CValue = CryptoConvertBB("LTC", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "LTC/EUR", CValue = CryptoConvertBB("LTC", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "LTC/PLN", CValue = CryptoConvertBB("LTC", "PLN") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "ETH/USD", CValue = CryptoConvertBB("ETH", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "ETH/EUR", CValue = CryptoConvertBB("ETH", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "ETH/PLN", CValue = CryptoConvertBB("ETH", "PLN") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "LSK/USD", CValue = CryptoConvertBB("LSK", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "LSK/EUR", CValue = CryptoConvertBB("LSK", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "LSK/PLN", CValue = CryptoConvertBB("LSK", "PLN") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "GAME/USD", CValue = CryptoConvertBB("GAME", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "GAME/EUR", CValue = CryptoConvertBB("GAME", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "GAME/PLN", CValue = CryptoConvertBB("GAME", "PLN") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "DASH/USD", CValue = CryptoConvertBB("DASH", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "DASH/EUR", CValue = CryptoConvertBB("DASH", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "DASH/PLN", CValue = CryptoConvertBB("DASH", "PLN") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BTG/USD", CValue = CryptoConvertBB("BTG", "USD") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BTG/EUR", CValue = CryptoConvertBB("BTG", "EUR") });
            CurrenciesBitBay.Add(new CurrencyModel() { CName = "BTG/PLN", CValue = CryptoConvertBB("BTG", "PLN") });

            CurrenciesBitStamp = new List<CurrencyModel>();
            CurrenciesBitStamp.Add(new CurrencyModel() { CName = "BTC/USD", CValue = CryptoConvertGE("btc", "usd") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "BTC/EUR", CValue = CryptoConvertBS("btc", "eur") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "BTC/PLN", CValue = CryptoConvertBS("BTC", "PLN") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "LTC/USD", CValue = CryptoConvertBS("LTC", "USD") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "LTC/EUR", CValue = CryptoConvertBS("LTC", "EUR") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "LTC/PLN", CValue = CryptoConvertBS("LTC", "PLN") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "ETH/USD", CValue = CryptoConvertBS("eth", "usd") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "ETH/EUR", CValue = CryptoConvertBS("eth", "eur") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "ETH/PLN", CValue = CryptoConvertBS("ETH", "PLN") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "XRP/USD", CValue = CryptoConvertBS("XRP", "USD") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "XRP/EUR", CValue = CryptoConvertBS("XRP", "EUR") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "XRP/PLN", CValue = CryptoConvertBS("XRP", "PLN") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "BCH/USD", CValue = CryptoConvertBS("BCH", "USD") });
            //CurrenciesBitStamp.Add(new CurrencyModel() { CName = "BCH/EUR", CValue = CryptoConvertBS("BCH", "EUR") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "BCH/PLN", CValue = CryptoConvertBS("BCH", "PLN") });
            
            CurrenciesLivecoin = new List<CurrencyModel>();
            CurrenciesLivecoin.Add(new CurrencyModel() { CName = "BTC/USD", CValue = CryptoConvertLC("BTC", "USD") });
            CurrenciesLivecoin.Add(new CurrencyModel() { CName = "BTC/EUR", CValue = CryptoConvertLC("BTC", "EUR") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "BTC/PLN", CValue = CryptoConvertBS("BTC", "PLN") });
            //CurrenciesLivecoin.Add(new CurrencyModel() { CName = "LTC/USD", CValue = CryptoConvertLC("LTC", "USD") });
            //CurrenciesLivecoin.Add(new CurrencyModel() { CName = "LTC/EUR", CValue = CryptoConvertLC("LTC", "EUR") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "LTC/PLN", CValue = CryptoConvertBS("LTC", "PLN") });
            //CurrenciesLivecoin.Add(new CurrencyModel() { CName = "ETH/USD", CValue = CryptoConvertLC("ETH", "USD") });
            //CurrenciesLivecoin.Add(new CurrencyModel() { CName = "ETH/EUR", CValue = CryptoConvertLC("ETH", "EUR") });
            ////CurrenciesBitStamp.Add(new CurrencyModel() { CName = "ETH/PLN", CValue = CryptoConvertBS("ETH", "PLN") });

            CurrenciesGemini = new List<CurrencyModel>();
            CurrenciesGemini.Add(new CurrencyModel() { CName = "BTC/USD", CValue = CryptoConvertGE("btc", "usd") });
            //CurrenciesGemini.Add(new CurrencyModel() { CName = "BTC/EUR", CValue = CryptoConvertGE("btc", "eur") });
            //CurrenciesGemini.Add(new CurrencyModel() { CName = "BTC/PLN", CValue = CryptoConvertGE("btc", "pln") });
            //CurrenciesGemini.Add(new CurrencyModel() { CName = "LTC/USD", CValue = CryptoConvertGE("ltc", "usd") });
            //CurrenciesGemini.Add(new CurrencyModel() { CName = "LTC/EUR", CValue = CryptoConvertGE("ltc", "eur") });
            //CurrenciesGemini.Add(new CurrencyModel() { CName = "LTC/PLN", CValue = CryptoConvertGE("ltc", "pln") });
            CurrenciesGemini.Add(new CurrencyModel() { CName = "ETH/USD", CValue = CryptoConvertGE("eth", "usd") });
            // CurrenciesGemini.Add(new CurrencyModel() { CName = "ETH/EUR", CValue = CryptoConvertGE("eth", "eur") });
            //CurrenciesGemini.Add(new CurrencyModel() { CName = "ETH/PLN", CValue = CryptoConvertBS("eth", "pln") });

            Exchanges = new List<ExchangeModel>();
            Exchanges.Add(new ExchangeModel() { EName = "BitBay", ECur = CurrenciesBitBay });
            Exchanges.Add(new ExchangeModel() { EName = "BitStamp", ECur = CurrenciesBitStamp });
            Exchanges.Add(new ExchangeModel() { EName = "Livecoin", ECur = CurrenciesLivecoin });
            Exchanges.Add(new ExchangeModel() { EName = "Gemini", ECur = CurrenciesGemini });
        }
    }
}
