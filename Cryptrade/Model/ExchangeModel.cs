﻿using System.Collections.Generic;

namespace Cryptrade
{
    public class ExchangeModel
    {
        public string EName { get; set; }
        public int EId { get; set; }
        public IList<CurrencyModel> ECur { get; set; }
        public IList<CurrencyModel> ECurValues { get; set; }
    }
}
