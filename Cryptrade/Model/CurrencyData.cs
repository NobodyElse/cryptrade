﻿using System;
using System.Collections.Generic;

namespace Cryptrade.Model
{
    public class CurrencyData
    {
        public CurrencyData()
        {
        }

        public static IList<CurrencyModel> Currencies { get; private set; }
         
        static CurrencyData()
        {
            Currencies = new List<CurrencyModel>();
            Currencies.Add(new CurrencyModel() { CName = "USD", CId = 1, CValue = 111 });
            Currencies.Add(new CurrencyModel() { CName = "PLN", CId = 2, CValue = 112 });
            Currencies.Add(new CurrencyModel() { CName = "YEN", CId = 3, CValue = 113 });
            Currencies.Add(new CurrencyModel() { CName = "EUR", CId = 4, CValue = 114 });  
        }
    }
}
