﻿using System;
using System.Collections.Generic;
using Microcharts;

namespace Cryptrade.Model
{
    public class ChartModel
    {
        public string Exchange { get; set; }
        public IList<ChartTimeModel> ChartTime { get; set; }
    }
}
