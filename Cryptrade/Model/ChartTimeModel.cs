﻿using Microcharts;

namespace Cryptrade.Model
{
    public class ChartTimeModel
    {
        public Chart Chart { get; set; }
        public string Time { get; set; }
    }
}
