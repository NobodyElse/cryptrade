﻿using System;
using System.ComponentModel;

namespace Cryptrade
{
    public class CurrencyModel
    {
        public string CName { get; set; }
        public int CId { get; set; }
        public double CValue { get; set; }
    }
}
