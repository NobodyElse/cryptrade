﻿using System;
using System.Collections.Generic;
using System.Net;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cryptrade
{
    public partial class ConvertNbp
    {
        [JsonProperty("table")]
        public string Table { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("rates")]
        public List<Rate> Rates { get; set; }
    }

    public partial class Rate
    {
        [JsonProperty("no")]
        public string No { get; set; }

        [JsonProperty("effectiveDate")]
        public System.DateTimeOffset EffectiveDate { get; set; }

        [JsonProperty("mid")]
        public double Mid { get; set; }
    }

    public partial class ConvertNbp
    {
        public static ConvertNbp FromJson(string json) => JsonConvert.DeserializeObject<ConvertNbp>(json, Cryptrade.ConverterNBP.Settings);
    }

    public static class SerializeNBP
    {
        public static string ToJson(this ConvertNbp self) => JsonConvert.SerializeObject(self, Cryptrade.ConverterNBP.Settings);
    }

    internal class ConverterNBP
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
