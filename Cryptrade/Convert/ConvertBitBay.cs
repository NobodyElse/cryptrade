﻿namespace Cryptrade
{
    using Newtonsoft.Json;

    public partial class ConvertBitBay
    {
        [JsonProperty("max")]
        public double Max { get; set; }

        [JsonProperty("min")]
        public double Min { get; set; }

        [JsonProperty("last")]
        public long Last { get; set; }

        [JsonProperty("bid")]
        public double Bid { get; set; }

        [JsonProperty("ask")]
        public double Ask { get; set; }

        [JsonProperty("vwap")]
        public long Vwap { get; set; }

        [JsonProperty("average")]
        public long Average { get; set; }

        [JsonProperty("volume")]
        public double Volume { get; set; }
    }

    public partial class ConvertBitBay
    {
        public static ConvertBitBay FromJson(string json) => JsonConvert.DeserializeObject<ConvertBitBay>(json, Cryptrade.ConverterBB.Settings);
    }

    public static class SerializeBB
    {
        public static string ToJson(this ConvertBitBay self) => JsonConvert.SerializeObject(self, Cryptrade.ConverterBB.Settings);
    }

    public class ConverterBB
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
