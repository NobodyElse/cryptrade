﻿using System;
using System.Collections.Generic;
using System.Net;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cryptrade
{
    public partial class ConvertGemini
    {
        [JsonProperty("bid")]
        public string Bid { get; set; }

        [JsonProperty("ask")]
        public string Ask { get; set; }

        [JsonProperty("volume")]
        public Volume Volume { get; set; }

        [JsonProperty("last")]
        public string Last { get; set; }
    }

    public partial class Volume
    {
        [JsonProperty("BTC")]
        public string Btc { get; set; }

        [JsonProperty("USD")]
        public string Usd { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
    }

    public partial class ConvertGemini
    {
        public static ConvertGemini FromJson(string json) => JsonConvert.DeserializeObject<ConvertGemini>(json, Cryptrade.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ConvertGemini self) => JsonConvert.SerializeObject(self, Cryptrade.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
