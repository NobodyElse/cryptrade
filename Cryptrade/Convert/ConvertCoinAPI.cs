﻿using System;
using System.Collections.Generic;
using System.Net;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace Cryptrade
{
    public partial class ConvertCoinApi
    {
        [JsonProperty("Response")]
        public string Response { get; set; }

        [JsonProperty("Type")]
        public long Type { get; set; }

        [JsonProperty("Aggregated")]
        public bool Aggregated { get; set; }

        [JsonProperty("Data")]
        public List<Datum> Data { get; set; }

        [JsonProperty("TimeTo")]
        public long TimeTo { get; set; }

        [JsonProperty("TimeFrom")]
        public long TimeFrom { get; set; }

        [JsonProperty("FirstValueInArray")]
        public bool FirstValueInArray { get; set; }

        [JsonProperty("ConversionType")]
        public ConversionType ConversionType { get; set; }
    }

    public partial class ConversionType
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("conversionSymbol")]
        public string ConversionSymbol { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("time")]
        public long Time { get; set; }

        [JsonProperty("close")]
        public double Close { get; set; }

        [JsonProperty("high")]
        public float High { get; set; }

        [JsonProperty("low")]
        public double Low { get; set; }

        [JsonProperty("open")]
        public double Open { get; set; }

        [JsonProperty("volumefrom")]
        public double Volumefrom { get; set; }

        [JsonProperty("volumeto")]
        public double Volumeto { get; set; }
    }

    public partial class ConvertCoinApi
    {
        public static ConvertCoinApi FromJson(string json) => JsonConvert.DeserializeObject<ConvertCoinApi>(json, Cryptrade.ConverterCA.Settings);
    }

    public static class SerializeCA
    {
        public static string ToJson(this ConvertCoinApi self) => JsonConvert.SerializeObject(self, Cryptrade.ConverterCA.Settings);
    }

    internal class ConverterCA
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
