﻿namespace Cryptrade
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ConvertBitStamp
    {
        [JsonProperty("high")]
        public string High { get; set; }

        [JsonProperty("last")]
        public string Last { get; set; }

        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("bid")]
        public string Bid { get; set; }

        [JsonProperty("vwap")]
        public string Vwap { get; set; }

        [JsonProperty("volume")]
        public string Volume { get; set; }

        [JsonProperty("low")]
        public string Low { get; set; }

        [JsonProperty("ask")]
        public string Ask { get; set; }

        [JsonProperty("open")]
        public string Open { get; set; }
    }

    public partial class ConvertBitStamp
    {
        public static ConvertBitStamp FromJson(string json) => JsonConvert.DeserializeObject<ConvertBitStamp>(json, Cryptrade.ConverterBS.Settings);
    }

    public static class SerializeBS
    {
        public static string ToJson(this ConvertBitStamp self) => JsonConvert.SerializeObject(self, Cryptrade.ConverterBS.Settings);
    }

    internal class ConverterBS
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
