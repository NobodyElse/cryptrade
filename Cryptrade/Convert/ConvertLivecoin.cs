﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Cryptrade;
//
//    var convertLivecoin = ConvertLivecoin.FromJson(jsonString);

namespace Cryptrade
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ConvertLivecoin
    {
        [JsonProperty("cur")]
        public string Cur { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("last")]
        public double Last { get; set; }

        [JsonProperty("high")]
        public double High { get; set; }

        [JsonProperty("low")]
        public double Low { get; set; }

        [JsonProperty("volume")]
        public double Volume { get; set; }

        [JsonProperty("vwap")]
        public double Vwap { get; set; }

        [JsonProperty("max_bid")]
        public double MaxBid { get; set; }

        [JsonProperty("min_ask")]
        public double MinAsk { get; set; }

        [JsonProperty("best_bid")]
        public double BestBid { get; set; }

        [JsonProperty("best_ask")]
        public long BestAsk { get; set; }
    }

    public partial class ConvertLivecoin
    {
        public static ConvertLivecoin FromJson(string json) => JsonConvert.DeserializeObject<ConvertLivecoin>(json, Cryptrade.ConverterLC.Settings);
    }

    public static class SerializeLC
    {
        public static string ToJson(this ConvertLivecoin self) => JsonConvert.SerializeObject(self, Cryptrade.ConverterLC.Settings);
    }

    internal class ConverterLC
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
