﻿using System.Collections.Generic;
using Cryptrade.Model;
using Cryptrade.ViewModels;
using Microcharts;

namespace Cryptrade
{
    public class ChartViewModel : ViewModelBase
    {
        public IList<Entry> Entries { get { return ChartData.Entries30D; } }
        public IList<ChartModel> Charts { get { return ChartData.Charts; } }

        ChartModel selectedChart;
        public ChartModel SelectedChart
        {
            get { return selectedChart; }
            set
            {
                if (selectedChart != value)
                {
                    selectedChart = value;
                    OnPropertyChanged();
                }
            }
        }

        ChartTimeModel selectedChartTime;
        public ChartTimeModel SelectedChartTime
        {
            get { return selectedChartTime; }
            set
            {
                if (selectedChartTime != value)
                {
                    selectedChartTime = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
