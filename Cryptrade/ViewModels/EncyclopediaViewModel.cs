﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace Cryptrade.ViewModels
{
    public class EncyclopediaViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public EncyclopediaViewModel()
            {
                MyItemsSource = new ObservableCollection<View>()
                {
                new WebView { 
                    Source = new UrlWebViewSource
                    {
                        Url = "https://pl.wikipedia.org/wiki/Bitcoin"
                    }
                },
                new WebView {
                    Source = new UrlWebViewSource
                    {
                        Url = "https://pl.wikipedia.org/wiki/Blockchain"
                    }
                }
                };
                
                MyCommand = new Command(() =>
                {
                    Debug.WriteLine("Position selected.");
                });
            }
            
        ObservableCollection<View> _myItemsSource;
        public ObservableCollection<View> MyItemsSource
            {   
                set {
                _myItemsSource = value;
                OnPropertyChanged("MyItemsSource");
                }
                get {
                    return _myItemsSource;
                }
            }

            public Command MyCommand { protected set; get; }

            protected virtual void OnPropertyChanged(string propertyName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            
        }
    }

