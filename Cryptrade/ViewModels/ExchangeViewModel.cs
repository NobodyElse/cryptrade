﻿using System.Collections.Generic;
using Cryptrade.Model;
using Cryptrade.ViewModels;

namespace Cryptrade
{
    public class ExchangeViewModel : ViewModelBase
    {
        public IList<ExchangeModel> Exchanges { get { return ExchangeData.Exchanges; } }

        ExchangeModel selectedExchange;
        public ExchangeModel SelectedExchange
        {
            get { return selectedExchange; }
            set
            {
                if (selectedExchange != value)
                {
                    selectedExchange = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
