﻿using Xamarin.Forms;

namespace Cryptrade
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new Pages.CryptradePage());
        }

        protected override void OnStart()
        {
            //var conv = await RestService.CheckBTCUSD(restService);
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


    }
}
