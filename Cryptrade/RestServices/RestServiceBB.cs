﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cryptrade.RestServices
{
    public class RestServiceBB
    {
        public async Task<ConvertBitBay> CheckCryptoFiatAsyncBB(string crypto, string fiat)
        {
            HttpClient client = new HttpClient();
            string restUrl = "https://bitbay.net/API/Public/" + crypto + fiat + "/ticker.json";
            HttpResponseMessage responseGet = await client.GetAsync(restUrl);
            if (responseGet.IsSuccessStatusCode)
            {
                var response = await responseGet.Content.ReadAsStringAsync();
                ConvertBitBay currencies = ConvertBitBay.FromJson(response);
                return currencies;
            }
            else
            {
                Debug.WriteLine("CheckCryptoFiatAsyncBB failed");
                return null;
            }
        }
       
    }
}