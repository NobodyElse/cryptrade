﻿using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cryptrade.RestServices
{
    public class RestServiceBS
    {
        public async Task<ConvertBitStamp> CheckCryptoFiatAsyncBS(string crypto, string fiat)
        {
            HttpClient client = new HttpClient();
            string restUrl = "https://www.bitstamp.net/api/v2/ticker/" + crypto + fiat + "/";
            HttpResponseMessage responseGet = await client.GetAsync(restUrl);
            if (responseGet.IsSuccessStatusCode)
            {
                var response2 = await responseGet.Content.ReadAsStringAsync();
                ConvertBitStamp currencies = ConvertBitStamp.FromJson(response2);
                return currencies;
            }
            else
            {
                Debug.WriteLine("CheckCryptoFiatAsyncBS failed");
                return null;
            }
        }
    }
}