﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cryptrade.RestServices
{
    public class RestServiceLC
    {
        public async Task<ConvertLivecoin> CheckCryptoFiatAsyncLC(string crypto, string fiat)
        {
            HttpClient client = new HttpClient();
            string restUrl = "https://api.livecoin.net/exchange/ticker?currencyPair=" + crypto + "/" + fiat;
            HttpResponseMessage responseGet = await client.GetAsync(restUrl);
            if (responseGet.IsSuccessStatusCode)
            {
                var response = await responseGet.Content.ReadAsStringAsync();
                ConvertLivecoin currencies = ConvertLivecoin.FromJson(response);
                return currencies;
            }
            else
            {
                Debug.WriteLine("CheckCryptoFiatAsyncLC failed");
                return null;
            }
        }
       
    }
}