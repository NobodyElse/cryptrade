﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cryptrade.RestServices
{
    public class RestServiceGE
    {
        public async Task<ConvertGemini> CheckCryptoFiatAsyncGE(string crypto, string fiat)
        {
            HttpClient client = new HttpClient();
            string restUrl = "https://api.gemini.com/v1/pubticker/" + crypto + fiat;
            HttpResponseMessage responseGet = await client.GetAsync(restUrl);
            if (responseGet.IsSuccessStatusCode)
            {
                var response = await responseGet.Content.ReadAsStringAsync();
                ConvertGemini currencies = ConvertGemini.FromJson(response);
                return currencies;
            }
            else
            {
                Debug.WriteLine("CheckCryptoFiatAsyncGE failed");
                return null;
            }
        }
       
    }
}