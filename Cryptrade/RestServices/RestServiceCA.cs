﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cryptrade.RestServices
{
    public class RestServiceCA
    {
        public async Task<ConvertCoinApi> CheckCryptoFiatAsyncCA(string crypto, string fiat, int days)
        {
            HttpClient client = new HttpClient();
            string restUrl = "https://min-api.cryptocompare.com/data/histoday?fsym="
                + crypto + "&tsym=" + fiat + "&limit="+ days + "&aggregate=1";
            HttpResponseMessage responseGet = await client.GetAsync(restUrl);
            if (responseGet.IsSuccessStatusCode)
            {
                var response = await responseGet.Content.ReadAsStringAsync();
                ConvertCoinApi currencies = ConvertCoinApi.FromJson(response);
                return currencies;
            }
            else
            {
                Debug.WriteLine("CheckCryptoFiatAsyncCA failed");
                return null;
            }
        }

        public async Task<ConvertCoinApi> CheckCryptoFiatAsyncCAHours(string crypto, string fiat)
        {
            HttpClient client = new HttpClient();
            string restUrl = "https://min-api.cryptocompare.com/data/histohour?fsym="
                + crypto + "&tsym=" + fiat + "&limit=24&aggregate=1";
            HttpResponseMessage responseGet = await client.GetAsync(restUrl);
            if (responseGet.IsSuccessStatusCode)
            {
                var response = await responseGet.Content.ReadAsStringAsync();
                ConvertCoinApi currencies = ConvertCoinApi.FromJson(response);
                return currencies;
            }
            else
            {
                Debug.WriteLine("CheckCryptoFiatAsyncCAHours failed");
                return null;
            }
        }

       
    }
}