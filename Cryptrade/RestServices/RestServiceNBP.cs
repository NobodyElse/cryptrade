﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cryptrade.RestServices
{
    public class RestServiceNBP
    {
        public async Task<ConvertNbp> CheckNBP(string currency)
        {
            HttpClient client = new HttpClient();
            string restUrl = "http://api.nbp.pl/api/exchangerates/rates/a/" + currency + "?format=json";
            HttpResponseMessage responseGet = await client.GetAsync(restUrl);
            if (responseGet.IsSuccessStatusCode)
            {
                var response = await responseGet.Content.ReadAsStringAsync();
                ConvertNbp currencies = ConvertNbp.FromJson(response);
                return currencies;
            }
            else
            {
                Debug.WriteLine("CheckNBP failed");
                return null;
            }
        }
       
    }
}